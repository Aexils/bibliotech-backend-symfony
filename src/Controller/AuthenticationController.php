<?php

namespace App\Controller;

use App\Entity\Library;
use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Serializer;
use Twig\Environment;


class AuthenticationController extends AbstractController
{
    /** @var Environment */
    private $renderer;

    /**
     * @Route("/api/users/inscription", name="inscription", methods={"POST"})
     */
    public function inscription(EntityManagerInterface $em, Request $request)
    {
        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }

        $pseudo = $parametersAsArray["pseudo"];
        $email = $parametersAsArray["email"];
        $pwd = $parametersAsArray["password"];

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('App:User');
        $testEmail = $repository->findOneBy(['email' => $email]);
        $testPseudo = $repository->findOneBy(['pseudo' => $pseudo]);

        if ($testEmail && $testPseudo) {
            return new Response("pas bien");
        } else {
            $user = new User();
            $user->setToken($this->generateToken());
            $user->setEmail($email);
            $user->setPseudo($pseudo);
            $hash = password_hash($pwd, PASSWORD_BCRYPT);
            $user->setPassword($hash);

            $em->persist($user);
            $em->flush();

            return new JsonResponse([
                "response" => "Bien inscrit"
            ]);
        }
    }

    /**
     * @Route("/api/users/connexion", name="connexion", methods={"POST"})
     */
    public function connexion(EntityManagerInterface $em, Request $request)
    {
        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }

        $email = $parametersAsArray["email"];
        $pwd = $parametersAsArray["password"];

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('App:User');
        if ($repository->findOneBy(['email' => $email])) {
            $user = $repository->findOneBy(['email' => $email]);
        } else if ($user = $repository->findOneBy(['pseudo' => $email])) {
            $user = $repository->findOneBy(['pseudo' => $email]);
        } else {
            return $this->json('Erreur lors de l\'authentification', 500);
        }
        $hash = $user->getPassword();
        $mdpVerify = password_verify($pwd, $hash);

        if ($mdpVerify == 1) {
            $user->setIsConnected(1);
            $em->persist($user);
            $em->flush();
            return new JsonResponse([
                "email" => $user->getEmail(),
                "pseudo" => $user->getPseudo(),
                "isConnected" => $user->getIsConnected(),
                "token" => $user->getToken()
            ]);
        } else {
            return new Response("erreur, mot de passe incorrect", 500);
        }
    }

    /**
     * @Route("/api/bibliotheque/{token_user}", name="bibliotheque", methods={"GET"})
     */
    public function bibliotheque(EntityManagerInterface $em, Request $request, $token_user, UserRepository $userRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $repoUser = $em->getRepository('App:User');
        $user = $repoUser->findOneBy(['token' => $token_user]);

        $libraries = $user->getLibrary(); // here we get all libraries which are in relation with user #4, you might want to rename the function as it returns ArrayCollection of Libraries, not Library
        $bookIds = array();
        foreach ($libraries as $library) {
            $bookIds[] = "https://www.googleapis.com/books/v1/volumes/" . $library->getIdBook();
        }

        return new JsonResponse([
            "books" => $bookIds
        ]);

        /*       return $this->json($userRepository->findOneBy(["token" => $token_user]), 200, [], ['groups' => 'user:json']);*/
    }

    /**
     * @Route("/api/bibliotheque/{token_user}/ajouter/{id_book}", name="bibliothequeAjouter", methods={"GET"})
     */
    public function bibliothequeAjouter(EntityManagerInterface $em, Request $request, $token_user, $id_book)
    {
        $em = $this->getDoctrine()->getManager();
        $repoUser = $em->getRepository('App:User');
        $user = $repoUser->findOneBy(['token' => $token_user]);

        $library = new Library();
        $library->setIdBook($id_book);

        $user->addLibrary($library);
        $em->persist($library);
        $em->persist($user);
        $em->flush();

        return new Response("Bien ajouté");
    }

    public function generateToken()
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }

    /**
     * @Route("/api/users/mot_de_passe_oublie", name="api_mot_de_passe_oublie", methods={"POST"})
     */
    public function MotDePasseOublie(Request $request, \Swift_Mailer $mailer)
    {
        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }
        $email = $parametersAsArray["email"];

        $em = $this->getDoctrine()->getManager();
        $repository = $em->getRepository('App:User');

        if ($user = $repository->findOneBy(['email' => $email])) {
            $user->setResetToken($this->generateToken());
            $token = $user->getResetToken();
            $em->persist($user);
            $em->flush();
            $message = (new \Swift_Message('Mot de passe oublié ?'))
                ->setFrom('contact@books.fr')
                ->setTo($user->getEmail())
                ->setBody($this->renderView('authentication/email_mot_de_passe_oublie.html.twig', [
                    'resetToken' => $token
                ]), 'text/html');
            $mailer->send($message);
        } else {
            return $this->json("Email incorrect", 500);
        }

        return $this->json("Email envoyé", 200);
    }

    /**
     * @Route("api/users/reset_mot_de_passe/{resetToken}", name="api_reset_mot_de_passe")
     */
    public function ResetMotDePasse(Request $request, $resetToken)
    {
        $form = $this->createFormBuilder()
            ->add('password', PasswordType::class)
            ->getForm();

        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($form->isSubmitted() && $form->isValid()) {
            $repository = $em->getRepository(User::class);
            $user = $repository->findOneBy(['resetToken' => $resetToken]);
            $mdp = $form->get('password')->getData();
            $hash = Password_hash($mdp, PASSWORD_BCRYPT);
            $user->setResetToken(null);
            $user->setPassword($hash);
            $date = new \DateTime('now');
            $user->setUpdatedAt($date);
            $em->flush();
            $this->addFlash(
                'mdpchanged',
                'Votre mot de passe a bien été modifié'
            );
            return $this->render('authentication/reset_password.html.twig', ['form' => $form->createView(),
                'resetToken' => $resetToken]);
        }

        return $this->render('authentication/reset_password.html.twig', ['form' => $form->createView(),
            'resetToken' => $resetToken]);
    }
}