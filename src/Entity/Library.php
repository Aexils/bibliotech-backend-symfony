<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LibraryRepository")
 */
class Library
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     * @Groups("user:json")
     */
    private $id_book;

    /**
     * @ORM\Id()
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="Library")
     */
    private $user;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    public function getIdBook(): ?string
    {
        return $this->id_book;
    }

    public function setIdBook(string $id_book): self
    {
        $this->id_book = $id_book;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
